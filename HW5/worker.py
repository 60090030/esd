import pika
from flask import request, jsonify
import logging
import json
import mysql.connector
from mysql.connector import Error
import re

#Read the text file
friends = []
file = "friends.txt"
with open(file) as f:
    x = f.read().splitlines()
for i in x:
    friends.append(json.loads(str(i)))

#Connect to server
print("[x] Connecting to server ...")
connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.queue_declare(queue='task_queue', durable=True)

print("[x] Waiting for Message.")

def callback(ch,method,properties,body):
    print("[x] Received %s" % body)
    temp = str(body)
    temp = temp[2:-1]
    a = re.split(r'\s',temp)
    fid = a[2][:-7]
    fn = a[4][:-8].split('"')
    
    fn = fn[1]
    ft = a[6][:-1].split('"')
    
    ft = ft[1]
    

    try:
        mydb = mysql.connector.connect(
        host = "localhost",
        user = "Pong",
        passwd = "1234567890",
        database="esdhw")

        mycursor = mydb.cursor()

        sql = "insert into friends (Friend_ID,Name,Track) values (%s,%s,%s)"
        val = (int(fid),fn,ft)
        
        mycursor.execute(sql,val)
        mydb.commit()
    except Error as e:
        print("Error: ",e)
        
    with open("friends.txt", "a") as file:
            file.write("\n" + str(temp))

    print("[x] Done")

    ch.basic_ack(delivery_tag=method.delivery_tag)

channel.basic_qos(prefetch_count=1) #limit the no. of unacknowledge messages
channel.basic_consume(queue='task_queue',
                     on_message_callback=callback)
channel.start_consuming()
    
