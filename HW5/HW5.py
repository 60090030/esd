import flask

import pika
from flask import request, jsonify
import logging
import json

friends = []
file = "friends.txt"
with open(file) as f:
    x = f.read().splitlines()

for i in x:
    friends.append(json.loads(str(i)))

#print(friends)

logging.basicConfig(filename='app.log',level=logging.DEBUG
                    ,format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

app = flask.Flask(__name__)
app.config["DEBUG"] = Trueidlefriends = [{'id' : 0,
           'name' : 'Window',
           'track' : 'Enterprise_system'},
        {'id' : 1,
           'name' : 'Beam',
           'track' : 'ioT'},
        {'id' : 2,
           'name' : 'Manu',
           'track' : 'Intelligent_system'},
        {'id' : 3,
           'name' : 'Nince',
           'track' : 'Enterprise_system'},
        {'id' : 4,
           'name' : 'Arun',
           'track' : 'ioT'}
        ]




@app.route('/', methods=['GET'])
def home():
    return "<h1>ESD</h1><br><h1>Name: Pokpong Rujuirachartkul</h1>"

@app.route('/api/v1/resource/friends/all', methods=['GET'])
def api_all():
    friends = []
    with open(file) as f:
        z = f.read().splitlines()

    for i in z:
        friends.append(json.loads(str(i)))
        
    return jsonify(friends)

@app.route('/api/v1/resource/friends_id', methods=['GET'])
def api_id():
    friends = []
    with open(file) as f:
        z = f.read().splitlines()

    for i in z:
        friends.append(json.loads(str(i)))
        
    if('id' in request.args):
        id = int(request.args['id'])
    else:
        return "Error : No id provided. Please specify an id"
    result = []

    for friend in friends:
        if friend['id'] == id:
            result.append(friend)

    return jsonify(result)
@app.route('/api/v1/resource/friends_name', methods=['GET'])
def api_name():
    friends = []
    with open(file) as f:
        z = f.read().splitlines()

    for i in z:
        friends.append(json.loads(str(i)))
        
    if('name' in request.args):
        name = request.args['name']
    else:
        return "Error : No name provided. Please specify name"
    result = []

    for friend in friends:
        if friend['name'] == name:
            result.append(friend)

    return jsonify(result)

@app.route('/api/v1/resource/friends_track', methods=['GET'])
def api_track():
    friends = []
    with open(file) as f:
        z = f.read().splitlines()

    for i in z:
        friends.append(json.loads(str(i)))

    if('track' in request.args):
        track = request.args['track']
    else:
        return "Error : No track provided. Please specify track"
    result = []

    for friend in friends:
        if friend['track'] == track:
            result.append(friend)

    return jsonify(result)     

@app.route('/api/v1/resource/friends/add', methods=['GET'])
def api_add():
    
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='task_queue',durable=True)

    temp = ''
    name = ''
    id = 0
    track = ''
    a = 0
    if('name' in request.args):
        name = request.args['name']
        a+=1
    if('id' in request.args):
        id = request.args['id']
        a+=1
    if('track' in request.args):
        track = request.args['track']
        a+=1
    if(a==3):
        temp = "{\"id\" : "+str(int(id))+",\"name\" : \""+name+"\",\"track\" : \""+track+"\"}"
                 
    channel.basic_publish(exchange='',
                          routing_key='task_queue',
                          body = temp,
                          properties=pika.BasicProperties(delivery_mode=2,))
            
    
    connection.close()
    print("[x] Sent: %s"%temp)
    return "<h1>Successfully added!</h1>"

app.run()
