import requests
import json
import ast
from datetime import datetime

while(True):
    print("Current Weather Data")
    n = input("Enter the location: ")
    print("--------------------------")
    try:
        response = requests.get("https://api.openweathermap.org/data/2.5/weather?q="+n+"&appid=e5f182d43d4937602e0e6797b0ec068f")
        content = response.content
        x = json.loads(content.decode("utf-8").replace("'",'"'))

        sys = x['sys']
        print(x['name']+","+ sys['country'])
        
        wea = x['weather']
        weather = wea[0]
        print("Weather : "+ weather['main'])
        print("Weather description: "+ weather['description'])

        temp = x['main']
        print("Temperature : ",int(temp['temp'])/10, "°C")
        print("Feels like : ",int(temp['feels_like'])/10,"°C")
        print("Max : ",int(temp['temp_max'])/10,"°C")
        print("Min : ",int(temp['temp_min'])/10,"°C")
        print("Humidity : ",temp['humidity'],"%")
        print("Pressure : ",temp['pressure']," hpa")

        try:
            print("Visibility : ",x['visibility']/1000,"Km")
        except KeyError:
            print("Visibility : No data")
        
        srise = datetime.fromtimestamp(sys['sunrise']).strftime('%H:%M')
        sset = datetime.fromtimestamp(sys['sunset']).strftime('%H:%M')
        print("Sunrise : ", srise)
        print("Sunset : ", sset)

        wind = x['wind']
        print("Wind speed : ",wind['speed'],"m/h")

    except KeyError:
        print("City not found!!")
        
    print("==========================")
    
